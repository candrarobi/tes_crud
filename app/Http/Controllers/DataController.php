<?php

namespace App\Http\Controllers;

use App\Models\Data;
use Illuminate\Http\Request;
use DataTables;

class DataController extends Controller
{
    public function index()
    {
        $data = Data::get();
        if (request()->ajax()) {
            return datatables()->of($data) 
                ->addColumn('aksi', function ($data) {
                    $button = " <button class='edit btn  btn-warning' id='" . $data->id . "' >Edit</button>";
                    $button .= " <button class='hapus btn  btn-danger' id='" . $data->id . "' >Hapus</button>";
                    return $button;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
        return view('home');
    }

    public function store(Request $request)
    {
        $data = new Data();
        $data->name = $request->nama;
        $data->telp = $request->telp;
        $data->alamat = $request->alamat;
        $simpan = $data->save();
        if ($simpan) {
            return response()->json(['data' => $data, 'text' => 'data berhasi disimpan'], 200);
        } else {
            return response()->json(['data' => $data, 'text' => 'data berhasi disimpan']);
        }
    }

    public function edits(Request $request)
    {
        $id = $request->id;
        $data = Data::find($id);
        return response()->json(['data' => $data]);
    } 

    public function updates(Request $request)
    {
        $id = $request->id;
        $datas = [
            'name' => $request->nama,
            'telp' => $request->telp,
            'alamat' => $request->alamat
        ];
        $data = Data::find($id);
        $simpan = $data->update($datas);
        if ($simpan) {
            return response()->json(['text' => 'berhasil diubah'], 200);
        } else {
            return response()->json(['text' => 'Gagal diubah'], 422);
        }
    }

    public function hapus(Request $request)
    {
        $id = $request->id;
        $data = Data::find($id);
        $data->delete();
        return response()->json(['text' => 'berhasil dihapus'], 200);
    }

}

<?php

namespace App\Http\Controllers;
use App\Models\Data;
use Illuminate\Http\Request;
use DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function indexx()
    {
        $data = Data::get();
        if (request()->ajax()) {
            return datatables()->of($data)->make(true);
            
        }
        return view('home');
    }
}
